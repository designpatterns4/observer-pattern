package com.designpaterns;

public interface DisplayElement {
    public void display();
}
