package com.designpaterns;

public class StatisticsDisplay implements Observer, DisplayElement{
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum= 0.0f;
    private int numReadings;
    private Subject weatherData;

    public StatisticsDisplay(WeatherData weatherData) {
        this.weatherData= weatherData;
        this.weatherData.registerObserver(this);
    }

    public void display() {
        System.out.println("Avg/Max/Min Temperature = " + (tempSum/numReadings)
                + "/" + maxTemp + "/" + minTemp );
    }


    public void update(float temp, float humidity, float pressure) {
        tempSum += temp;
        numReadings++;
        if(temp > maxTemp) {
            maxTemp = temp;
        }
        if(temp < minTemp) {
            minTemp = temp;
        }
        display();
    }
}
