package com.designpaterns;

public interface Observer {
    public void update(float temp, float humidity, float pressure);
}
